#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "structs.h"
#include "tools.h"

#define MAX_DP_SIZE 10000
#define NUM_CLASSES 3
#define MAX_CLUSTERS 100

double Class1Mes(Point *_array, int _size)//remvoe seasonality?
{
	int i;
	double flat_fun[_size];
	for (i = 0; i < _size; i++) //funciton
		flat_fun[i] = (i % 2) + 1;
	
	return BetaCalc(_array, flat_fun, _size);
}

double Class2Mes(Point *_array, int _size) //monotonically increaseing 
{
	int i;
	double inc_fun[_size];
	for (i = 0; i < _size; i++) //monotonically increagin function
		inc_fun[i] = i + 1;
		
	return BetaCalc(_array, inc_fun, _size);
}

double Class3Mes(Point *_array, int _size)
{
	int i;
	double dec_fun[_size];
	int inx = _size - 1;
	for (i = 0; i < _size; i++) //monotonically decreasing function
		dec_fun[i] = inx--;
		
	return BetaCalc(_array, dec_fun, _size);
}

//main entry
int main(int argc, char *argv[]) 
{

	double * prob_matrix[MAX_DP_SIZE];
	double **ecom_gain = GenEcomGain(NUM_CLASSES);
	
	FILE* dp_file;
	if (( dp_file = fopen(argv[1], "r")) == NULL)
		return 1;
		
	char line[512];
	Point * dp;
	int inx;
	int clu_num = -1;
	while(fgets ( line, sizeof line, dp_file ) != NULL)
	{
		if (line[0] == '#')
		{
			if (clu_num > -1)
			{
				prob_matrix[clu_num] = new double[NUM_CLASSES];
				prob_matrix[clu_num][0] = Class1Mes(dp, inx);
				prob_matrix[clu_num][1] = Class2Mes(dp, inx);
				prob_matrix[clu_num][2] = Class3Mes(dp, inx);
			
			}
			clu_num++;
			dp = new Point[MAX_DP_SIZE];
			inx = 0;
			continue;
		}
		char* field;
		field = strtok(&line[0],",");
		dp[inx].dim_1 = atof(field);
		field = strtok(NULL,",");
		dp[inx].dim_2 = atof(field);
		inx++;
		if (inx == MAX_DP_SIZE)
			exit(1);
	}
	fclose(dp_file);
	
	int i, j;
	double total = 0;
	double min = prob_matrix[0][0];
	for (i = 0; i < clu_num; i++)		// find the min
		for (j = 0; j < NUM_CLASSES; j++)
			if (prob_matrix[i][j] < min)
				min = prob_matrix[i][j];
	min = fabs(min);
		
	for (i = 0; i < clu_num; i++)		// get everything positive, get total
		for (j = 0; j < NUM_CLASSES; j++)
		{
			prob_matrix[i][j] += min;
			total += prob_matrix[i][j];
		}
		
	for (i = 0; i < clu_num; i++)		//normalize probablities
		for (j = 0; j < NUM_CLASSES; j++)
			prob_matrix[i][j] /= total;
			
	/*for (i = 0; i < NUM_CLASSES; i++)	
	{
		for (j = 0; j < clu_num; j++)
			printf("%f ",  prob_matrix[j][i]);
		printf("\n");
	}*/
	
	double *bayes = GenBayesDes(prob_matrix, ecom_gain, clu_num, NUM_CLASSES);
	for (i = 0; i < clu_num; i++)
		printf( "%f \n", bayes[i]);
	/*double **conf = GenConf(prob_matrix, bayes, clu_num, NUM_CLASSES);
	double exp_gain = ExpGain(ecom_gain, conf, clu_num, NUM_CLASSES);*/
	return 0;
} //end
