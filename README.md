# Application of Linear Manifold Clustering in High Dimensional Spaces 

Linear Manifold Clustering is very useful when trying to determine trend movement in financial
time series. While my application only used two dimensional data sets, it is clear the higher
degree of effectiveness of this technique over others. I was able to obtain nice clustering on
some example time series. I was then able to uses a Bayes decision rule based on a Beta
calculation to classify different sections of the time series into trend classes. The results show
that my approach was able to give a basic, but accurate, cluster and classification of data.
Many real-world applications could be created from this data. With more research and
development these techniques could serve as valuable and potentially profitable tools to a
market making trading operation.

References
1. "Beta Definition | Investopedia." Investopedia – The Web’s Largest Investing Resource. Web. 29 Dec. 2011. <http://www.investopedia.com/terms/b/beta.asp>.
2. Duda, Richard O., Peter E. Hart, and David G. Stork. Pattern Classification. New York: Wiley, 2001. Print.
3. "Gram-Schmidt Orthonormalization -- from Wolfram MathWorld." Wolfram MathWorld: The Web's Most Extensive Mathematics Resource. Web. 31 Dec. 2011. <http://mathworld.wolfram.com/Gram-SchmidtOrthonormalization.html>.
4. Haralick, Robert, and Rave Harpaz. "Linear Manifold Clustering in High Dimensional Spaces by Stochastic Search." Pattern Recognition 40.10 (2007): 2672-684. Web. 31 Dec. 2011. <http://acc6.its.brooklyn.cuny.edu/~rbharpaz/lmclusV3.pdf>.
5. Haralick, Robert, and Rave Harpaz. "Linear Manifold Clustering." In 4th International Conference on Machine Learning and Data Mining in Pattern Recognition(2005): 132-41. Web. 31 Dec. 2011. <http://acc6.its.brooklyn.cuny.edu/~rbharpaz/MLDM_lmclus.pdf>.
6. "Thomson Reuters | Thomson Reuters Tick History | Financial." Thomson Reuters | Home. Web. 31 Dec. 2011. <http://thomsonreuters.com/products_services/financial/financial_products/az/
tick_history/>.
