#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "structs.h"
#include "tools.h"

Separation FindSeparation(Point* _dp, int _dp_len, int _dim, int _samp_level)//data set, dimention, saml level
{
	Separation result;
	result.gamma = result.tau = -9999999;
	
	if (DEBUG_ON >= 1)
	{
		printf("\nRun FindSeparation. \n");
		printf("  Length = %d\n",_dp_len);
		printf("  Dim = %d\n", _dim );
		printf("  Sampling Level = %d\n", _samp_level);
	}
	double N = (int)(log (EPSILON) / log(1 - pow(1/(double)_samp_level, (double)_dim)));
	if (_dp_len < N) //use one as constant c
		N = _dp_len;
		
	if (DEBUG_ON >= 1)
		printf("Value of N = %f, DP length = %d\n", N, _dp_len);
	int i, j, k;
	for (i = 0; i < N; i++)
	{
		Point d_sam[_dim + 1]; //sample data points
		for (j = 0; j < _dim + 1; j++)
			d_sam[j] = _dp[rand() % _dp_len];
			
		Point orn = d_sam[rand() % (_dim + 1)];//element of d_sam
		Point * B;
		FormOrthonormalBasis(d_sam, orn, _dim + 1, B); //Gram-Schmidt Orthonormalization
		
		double distances[_dp_len - _dim + 1];
		
		int inx = 0;
		for (j = 0; i < _dp_len; j++)
		{
			int can = 0;
			for (k = 0; k < _dim + 1; k ++)
			{
				if (_dp[j].dim_1 == d_sam[k].dim_1
					&& _dp[j].dim_2 == d_sam[k].dim_2)
				{
					can = 1;
					break;
				}
			}
			if (can != 0)
				break;
			
			Point dis;
			dis.dim_1 = _dp[j].dim_1 - orn.dim_1;
			dis.dim_2 = _dp[j].dim_2 - orn.dim_2;
			double dis_p1 = sqrt(dis.dim_1  * dis.dim_1 
						+ dis.dim_2  * dis.dim_2);	
						
			double tmp_res[ _dim + 1 ];
			double dis_p2 = 0;
			for (k = 0; k < _dim + 1; k ++)
			{
				tmp_res[k] = dis.dim_1 * B[k].dim_1
						+ dis.dim_2 * B[k].dim_2;
			}
			for (k = 0; k < _dim + 1; k ++)	
				dis_p2 += tmp_res[k]  * tmp_res[k];
			
			dis_p2 = sqrt(dis_p2);
			distances[inx++] = pow(dis_p1, 2) - pow(dis_p2 , 2);
		
		}
		
		/*double min = distances[0];
		double max = min;
		for (j = 0; j < inx; j++)
		{
			if (distances[j] < min)
				min = distances[j];
			if (distances[j] > max)
				max = distances[j];
		}
		for (int y = 0; y<_dp_len; y++)
			printf (" ere %d, %f, %f\n", y, _dp[y].dim_1, _dp[y].dim_2);*/
		int hist_size = 100;//(int)((max - min) / 1000);
		Bin * hist;
		MakeHist(distances, inx, hist_size, hist);
		ErrorThres err_thres = FindMinErThres(hist, hist_size); //Kittler and Illingworth
		double good = EvalGoodness(err_thres, hist);

		if (good > result.gamma)
		{
			result.gamma = good;
			result.tau = err_thres.thres;
			result.theta = orn;
			result.beta = new Point[_dim + 1];
			for (j = 0; j < _dim + 1; j++)
				result.beta[j] = B[j];
				
			if (DEBUG_ON >= 1)
			{
				printf("\nGood Separation Found. \n");
				printf("  Gamma = %f\n",result.gamma);
				printf("  Tau = %f\n", result.tau );
				printf("  Theta = %f, %f\n", result.theta.dim_1, result.theta.dim_2);
				printf("  Beta = %d\n", j);
			
			}
		}	
	}
	if (DEBUG_ON >= 1)
		printf("Done FindSeparation. \n");
	return result;
}

//main entry point for this program
//built with g++ -g -o lmclus lmclus.c
//usage lmclus [datafile] [sampling_level] [max_liner_manifold_dim] [sensitivity threshold]
int main(int argc, char *argv[])
{	 
	srand ( time(NULL) );
	LabeledCluster labeled_clusters[MAX_CL_SIZE]; //list of labed clusters
	Point dp[MAX_DP_SIZE];
	int dp_ent = 0;
	
	FILE* dp_file;
	if (( dp_file = fopen(argv[1], "r")) == NULL)
		return 1;
		
	char line[512];
	while(fgets ( line, sizeof line, dp_file ) != NULL)
	{
		char* field;
		field = strtok(&line[0],",");
		dp[dp_ent].dim_1 = atof(field);
		field = strtok(NULL,",");
		dp[dp_ent].dim_2 = atof(field);
		dp_ent++;
		if (dp_ent == MAX_DP_SIZE)
			break;
	}
	fclose(dp_file);
	
	int samp_lev = atoi(argv[2]);
	int max_lm_dim = atoi(argv[3]);
	int sens_thres = atoi(argv[4]);

	if (DEBUG_ON >= 1)
	{
		printf("Initialized. \n");
		printf("  DP Filename = %s\n", argv[1]);
		printf("    Lines in file = %d\n", dp_ent);
		printf("  Sampling Level = %d\n", samp_lev);
		printf("  Max LM Dimension = %d\n", max_lm_dim);
		printf("  Sensitivity Threshold = %d\n", sens_thres);
	}
	
	int lb = 0; //cluster label;
	
	while (dp_ent > 0)
	{
		if (DEBUG_ON >= 1)
		{
			printf("\n==============================================\n");
			printf("Run %d-th iterations of main loop\n", lb);
			printf("  DP Remaining = %d\n", dp_ent);
		}
		Point *dp_pr;
		dp_pr = new Point[dp_ent];
		int i,j,k;
		for (i = 0; i < dp_ent; i++)
			dp_pr[i] = dp[i];  //deep copy
									
		int lm_dim = 1;
		
		int cl_len = dp_ent;
		Point *tmp_clr;
		tmp_clr = new Point[dp_ent];
		for (i = lm_dim; i < max_lm_dim; i++)
		{
			for(;;)
			{
				Separation separ = FindSeparation(dp, dp_ent, i, sens_thres);
				if (DEBUG_ON >= 1)
				{
					printf("\nClustering with separation:\n");
					printf("  Gamma = %f\n",separ.gamma);
					printf("  Tau = %f\n", separ.tau );
					printf("  Theta = %f, %f\n", separ.theta.dim_1, separ.theta.dim_2);
				}
				if (separ.gamma < sens_thres )
					break;
					
				cl_len = 0;
				tmp_clr = new Point[dp_ent];
				for (j = 0; j < dp_ent; j++)
				{
					Point dis; // D' := {x| x E D', ||x-o||*2 - ||BT(x-o)|| ^2 - T}
					dis.dim_1 = dp_pr[j].dim_1 - separ.theta.dim_1;
					dis.dim_2 = dp_pr[j].dim_2 - separ.theta.dim_2;
					double dis_p1 = sqrt(dis.dim_1  * dis.dim_1 
								+ dis.dim_2  * dis.dim_2);	
						
					double * tmp_res;
					tmp_res = new double[ i + 1 ];
					double dis_p2 = 0;
					for (k = 0; k < i + 1; k ++)
						tmp_res[k] = dis.dim_1 * separ.beta[k].dim_1
								+ dis.dim_2  * separ.beta[k].dim_2;
					for (k = 0; k < i + 1; k ++)	
						dis_p2 += tmp_res[k] * tmp_res[k];
					
					dis_p2 = sqrt(dis_p2);
					double distan = pow(dis_p1, 2) - pow(dis_p2 , 2) ;
			
					if ( distan >= separ.tau)
					{
						tmp_clr[cl_len++] = dp_pr[j];
					}		
					delete[] tmp_res;
				}
				
				lm_dim = i; // LmDim := k
				
				delete[] dp_pr;
				dp_pr = new Point[cl_len];
				for (i = 0; i < cl_len; i++)
					dp_pr[i] = tmp_clr[i];  //deep copy
			}
		}
		
		if (DEBUG_ON >= 1)
		{
			printf("Saving cluster %d\n", lb);
			printf("  Size = %d\n", dp_ent);	
		}
		labeled_clusters[lb].data_pts = new Point[cl_len]; // c := D'
		for (j = 0; j < cl_len; j++) //C : C U {ci}
			labeled_clusters[lb].data_pts[j] = dp_pr[j];			
		labeled_clusters[lb].dim = lm_dim; // dim := mDim
		labeled_clusters[lb].size = j;
		
		for (j = 0; j < dp_ent; j++) // D:=D-D'
		{
			for (k = 0; k < cl_len; k++)
			{
				if (dp[j].dim_1 == labeled_clusters[lb].data_pts[k].dim_1
					&& dp[j].dim_2 == labeled_clusters[lb].data_pts[k].dim_2)
				{
					int m;
					for (m = j; m < dp_ent - 1; m++)
						dp[m] = dp[m+1];
					dp_ent--;
					j--;
				}
			}
		}
		lb++; // i := i+1
		
		delete[] tmp_clr;
		delete[] dp_pr;
		if (DEBUG_ON >= 1)
		{
			printf("Done %d iterations of main loop\n", lb);
			printf("  DP Remaining = %d\n", dp_ent);
			printf("  Last Cluster Size = %d\n", cl_len);
		}
		
	}
	
	int i, j;
	for (i = 0; i< lb; i++)
	{
		printf("#Custer #%d. Dim = %d ================\n", i, labeled_clusters[i].dim);
		for (j = 0; j < labeled_clusters[i].size; j++)
			printf("%f,%f\n", labeled_clusters[i].data_pts[j].dim_1, labeled_clusters[i].data_pts[j].dim_2);
	}
	return 0;
}
